package com.skilleen.shippingservice;

import com.skilleen.shippingservice.dto.City;
import com.skilleen.shippingservice.dto.ShippingOrder;
import com.skilleen.shippingservice.entities.ShippingRequestEntity;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.util.concurrent.TimeUnit;

public class ShippingProcessor implements Processor {

    private static final Logger logger = LogManager.getLogger(ShippingProcessor.class);

    @Override
    public void process(Exchange exchange) {
        ShippingOrder order = exchange.getIn().getBody(ShippingOrder.class);
        logger.info("Creating Shipping Request... ");
        order.getDatePlaced().setTime(order.getDatePlaced().getTime() + TimeUnit.DAYS.toMillis(3));
        ShippingRequestEntity shippingRequestEntity = mapToEntityObject(order);
        logger.info(createReturnMessage(shippingRequestEntity));

        exchange.getIn().setBody(shippingRequestEntity);
    }

    private ShippingRequestEntity mapToEntityObject(ShippingOrder order) {
        ShippingRequestEntity entity = new ShippingRequestEntity();
        entity.setItemName(order.getName());
        entity.setPrice(order.getPrice());
        entity.setDateShipped(new Date(order.getDatePlaced().getTime()));
        entity.setStatus("In Progress");
        entity.setLocation(City.randomCity().toString());
        return entity;
    }

    private String createReturnMessage(ShippingRequestEntity order) {
        return "Order will be shipped from " + order.getLocation() + " on " + order.getDateShipped();
    }

}
