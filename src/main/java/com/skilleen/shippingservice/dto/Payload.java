package com.skilleen.shippingservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payload {

    private ShippingOrderChange before;
    private ShippingOrderChange after;
}
