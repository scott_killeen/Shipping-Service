package com.skilleen.shippingservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShippingOrder implements Serializable {

    private String name;
    private double price;
    private Date datePlaced;

    public ShippingOrder(String name, double price) {
        this.name = name;
        this.price = price;
        this.datePlaced = new Date();
    }
}
