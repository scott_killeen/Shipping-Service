package com.skilleen.shippingservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShippingOrderChange {

    private long shipping_id;
    private String item_name;
    private long price;
    private Date date_shipped;
    private String status;
    private String location;

    public void setDate_shipped(long date_shipped) {
        this.date_shipped = new Date(TimeUnit.DAYS.toMillis(date_shipped));
    }
}
