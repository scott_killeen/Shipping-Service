package com.skilleen.shippingservice.entities;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Date;

@Entity(name = "ShippingRequest")
@Data
public class ShippingRequestEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long shippingId;

    private String itemName;

    private double price;

    private Date dateShipped;

    private String status;

    private String location;

    @CreationTimestamp
    private Date createdAt;

}
