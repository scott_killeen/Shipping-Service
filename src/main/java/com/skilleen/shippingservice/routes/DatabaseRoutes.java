package com.skilleen.shippingservice.routes;

import com.skilleen.shippingservice.entities.ShippingRequestEntity;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class DatabaseRoutes extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("direct:insert-shipping-order")
            .to("jpa:" + ShippingRequestEntity.class.getName() + "?useExecuteUpdate=true");
    }
}
