package com.skilleen.shippingservice.routes;

import com.skilleen.shippingservice.dto.ChangeDataCaptureResponse;
import com.skilleen.shippingservice.dto.ShippingOrder;
import com.skilleen.shippingservice.ShippingProcessor;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class KafkaRoutes extends RouteBuilder {

    private static final Logger logger = LogManager.getLogger(KafkaRoutes.class);

    @Override
    public void configure() {
        from("kafka:scotts-topic?brokers=172.30.74.234:9092")
                .log("Message received from Kafka : ${body}");

        from("kafka:order-request?brokers=172.30.74.234:9092")
                .unmarshal().json(JsonLibrary.Jackson, ShippingOrder.class)
                .log("Message received from Kafka : ${body}")
                .process(new ShippingProcessor())
                .to("direct:insert-shipping-order");

        from("kafka:mysql.demodb.shipping_request?brokers=172.30.74.234:9092")
                .unmarshal().json(JsonLibrary.Jackson, ChangeDataCaptureResponse.class)
                .bean(this, "reportDataChange");
    }

    public void reportDataChange(Exchange exchange) {
        ChangeDataCaptureResponse change = exchange.getIn().getBody(ChangeDataCaptureResponse.class);
        if (change.getPayload().getBefore() != null) {
            logger.info("DETECTED DATABASE CHANGE");
            logger.info("WAS " + change.getPayload().getBefore());
            logger.info("NOW " + change.getPayload().getAfter());
            logger.info("Publishing change event for clients");
        }
    }
}
